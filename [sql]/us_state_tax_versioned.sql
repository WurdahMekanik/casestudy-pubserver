drop table us_state_tax;

create table us_state_tax (
  state             varchar(2)    not null,
  tax               binary_float  not null,
  workspace_id      varchar(20)   not null,
  branch_id         varchar(40)   not null,
  is_head           numeric(1)    not null,
  version_deleted   numeric(1)    not null,
  version_editable  numeric(1)    not null,
  pred_version      int,
  checkin_date      timestamp,
  asset_version     int           not null,
  primary key(state, asset_version)
);

CREATE INDEX US_STATE_TAX_CHECKIN_DATE ON US_STATE_TAX (CHECKIN_DATE);
CREATE INDEX US_STATE_TAX_WORKSPACE_ID ON US_STATE_TAX (WORKSPACE_ID);