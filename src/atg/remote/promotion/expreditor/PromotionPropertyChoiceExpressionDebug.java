package atg.remote.promotion.expreditor;

import atg.beans.DynamicBeanInfo;
import atg.beans.DynamicPropertyDescriptor;
import atg.core.util.ArraySorter;
import atg.core.util.SortOrder;
import atg.repository.groups.GroupProperty;
import atg.ui.commerce.pricing.PricingChoiceExpression;
import atg.ui.common.model.PropertyFilter;

public class PromotionPropertyChoiceExpressionDebug extends PricingChoiceExpression {
	public static final String CLASS_VERSION = "$Id: //product/DCS-UI/version/11.1/src/classes/atg/remote/promotion/expreditor/PromotionPropertyChoiceExpression.java#2 $$Change: 878942 $";
	private static final long serialVersionUID = 1L;
	private static PropertyFilter sPromotionPropertyFilter = null;

	public PromotionPropertyChoiceExpressionDebug() {
	}

	public PromotionPropertyChoiceExpressionDebug(DynamicBeanInfo pInfo) {
		super(pInfo);
	}

	public PropertyFilter getPropertyFilter() {
		if (sPromotionPropertyFilter == null)
			sPromotionPropertyFilter = new PromotionPropertyFilter();
		return sPromotionPropertyFilter;
	}

	protected DynamicPropertyDescriptor[] sortPropertyDescriptors(DynamicPropertyDescriptor[] pDescriptors) {
		if ((pDescriptors == null) || (pDescriptors.length <= 0)) {
			return pDescriptors;
		}

		ArraySorter.sort(pDescriptors, new SortOrder() {
			public int order(Object pObject1, Object pObject2) {
				DynamicPropertyDescriptor dpd1 = (DynamicPropertyDescriptor) pObject1;
				DynamicPropertyDescriptor dpd2 = (DynamicPropertyDescriptor) pObject2;

				if ((PromotionPropertyChoiceExpressionDebug.this.isContentGroupProperty(dpd1))
						&& (!(PromotionPropertyChoiceExpressionDebug.this.isContentGroupProperty(dpd2)))) {
					return 1;
				}
				if ((PromotionPropertyChoiceExpressionDebug.this.isContentGroupProperty(dpd2))
						&& (!(PromotionPropertyChoiceExpressionDebug.this.isContentGroupProperty(dpd1)))) {
					return -1;
				}

				return dpd1.getDisplaySortString().compareTo(dpd2.getDisplaySortString());
			}
		});
		return pDescriptors;
	}

	protected boolean isContentGroupProperty(DynamicPropertyDescriptor pDesc) {
		boolean isContentGroupProperty = false;

		DynamicPropertyDescriptor desc = pDesc;

		if (desc == null) {
			return false;
		}
		/*
		if (desc instanceof PromotionProperty) {
			System.out.println("Promo property (before) TESTESTESTESTESTESTTESTESTESTESTESTEST --+-- desc:" + desc);
			desc = ((PromotionProperty) desc).getPropertyDescriptor();
			System.out.println("Promo property (after) TESTESTESTESTESTESTTESTESTESTESTESTEST --+-- desc:" + desc);
		}
		if (desc instanceof PromotionExpressionContext.CoercableIsExpertPropertyDescriptor) {
			System.out.println("Coercable (before) TESTESTESTESTESTESTTESTESTESTESTESTEST --+-- desc:" + desc);
			desc = ((PromotionExpressionContext.CoercableIsExpertPropertyDescriptor) desc).getOriginalPropDescriptor();
			System.out.println("Coercable (after) TESTESTESTESTESTESTTESTESTESTESTESTEST --+-- desc:" + desc);
		}
		*/

		isContentGroupProperty = desc instanceof GroupProperty;
		return isContentGroupProperty;
	}

	public class PromotionPropertyFilter extends PricingChoiceExpression.PricingPropertyFilter {
		public PromotionPropertyFilter() {
			super();
		}

		public boolean acceptProperty(DynamicPropertyDescriptor pDpd) {
			System.out.println("");
			if (pDpd instanceof PromotionProperty) {
				PromotionProperty property = (PromotionProperty) pDpd;
				if (property.isMandatory()) {
					return true;
				}
			}
			return super.acceptProperty(pDpd);
		}
	}
}